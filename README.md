# HBase

Repository for HBase research

## Prerequisites

* Java 8

* Maven 4

* Python 3 (optional)

## Run

###### 1. Docker container

A container first run:

```
docker run -d -p 2181:2181 \
	-p 60010:60010 -p 60000:60000 \
	-p 60020:60020 -p 60030:60030 \
	-p 8080:8080 -p 8085:8085 \
	-p 9090:9090 -p 9095:9095 \
	--name hbase -h hbase \
	sixeyed/hbase-succinctly
```

Next runs:
```
docker start hbase
```

Stop:
```
docker stop hbase
```

Fill with test data:

```
docker exec -it hbase hbase shell /hbase-scripts/setup.hbsh
```

Open HBase shell:
```
docker exec -it hbase hbase shell
```

**IMPORTANT**: Add the record to /etc/hosts:
```
127.0.0.1   hbase
```

###### 2. Java client

Build:
```
cd hbase
mvn clean install
```

Run:
```
java -jar ./target/hbase-1.0-SNAPSHOT-jar-with-dependencies.jar 
```

Or from IntelliJ IDEA.

###### 3. Python client

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python hbase.py
```
package org.ustimov.hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        Configuration config = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(config);
        Table test_table = connection.getTable(TableName.valueOf("test_table"));
        Get get = new Get(Bytes.toBytes("rk1"));
        Result result = test_table.get(get);
        for (Cell cell : result.listCells()) {
            System.out.println(Bytes.toString(CellUtil.cloneFamily(cell)) + ":" + Bytes.toString(CellUtil.cloneQualifier(cell)) + " = " + Bytes.toString(CellUtil.cloneValue(cell)));
        }
    }
}
